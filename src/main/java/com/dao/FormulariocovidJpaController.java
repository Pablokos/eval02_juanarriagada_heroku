/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dao;

import com.dao.exceptions.NonexistentEntityException;
import com.dao.exceptions.PreexistingEntityException;
import com.entity.Formulariocovid;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @Author : Juan Pablo Arriagada
 */
public class FormulariocovidJpaController implements Serializable {

    public FormulariocovidJpaController() {
        
    }
    
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Formulariocovid formulariocovid) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(formulariocovid);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findFormulariocovid(formulariocovid.getRut()) != null) {
                throw new PreexistingEntityException("Formulariocovid " + formulariocovid + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Formulariocovid formulariocovid) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            formulariocovid = em.merge(formulariocovid);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = formulariocovid.getRut();
                if (findFormulariocovid(id) == null) {
                    throw new NonexistentEntityException("The formulariocovid with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Formulariocovid formulariocovid;
            try {
                formulariocovid = em.getReference(Formulariocovid.class, id);
                formulariocovid.getRut();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The formulariocovid with id " + id + " no longer exists.", enfe);
            }
            em.remove(formulariocovid);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Formulariocovid> findFormulariocovidEntities() {
        return findFormulariocovidEntities(true, -1, -1);
    }

    public List<Formulariocovid> findFormulariocovidEntities(int maxResults, int firstResult) {
        return findFormulariocovidEntities(false, maxResults, firstResult);
    }

    private List<Formulariocovid> findFormulariocovidEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Formulariocovid.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Formulariocovid findFormulariocovid(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Formulariocovid.class, id);
        } finally {
            em.close();
        }
    }

    public int getFormulariocovidCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Formulariocovid> rt = cq.from(Formulariocovid.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
