/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @Author : Juan Pablo Arriagada
 */
@Entity
@Table(name = "formulariocovid")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Formulariocovid.findAll", query = "SELECT f FROM Formulariocovid f"),
    @NamedQuery(name = "Formulariocovid.findByRut", query = "SELECT f FROM Formulariocovid f WHERE f.rut = :rut"),
    @NamedQuery(name = "Formulariocovid.findByNombres", query = "SELECT f FROM Formulariocovid f WHERE f.nombres = :nombres"),
    @NamedQuery(name = "Formulariocovid.findByApellidoPaterno", query = "SELECT f FROM Formulariocovid f WHERE f.apellidoPaterno = :apellidoPaterno"),
    @NamedQuery(name = "Formulariocovid.findByApellidoMaterno", query = "SELECT f FROM Formulariocovid f WHERE f.apellidoMaterno = :apellidoMaterno"),
    @NamedQuery(name = "Formulariocovid.findByCorreoElectronico", query = "SELECT f FROM Formulariocovid f WHERE f.correoElectronico = :correoElectronico")})
public class Formulariocovid implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "rut")
    private String rut;
    @Size(max = 2147483647)
    @Column(name = "nombres")
    private String nombres;
    @Size(max = 2147483647)
    @Column(name = "apellido_paterno")
    private String apellidoPaterno;
    @Size(max = 2147483647)
    @Column(name = "apellido_materno")
    private String apellidoMaterno;
    @Size(max = 2147483647)
    @Column(name = "correo_electronico")
    private String correoElectronico;

    public Formulariocovid() {
    }

    public Formulariocovid(String rut) {
        this.rut = rut;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rut != null ? rut.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Formulariocovid)) {
            return false;
        }
        Formulariocovid other = (Formulariocovid) object;
        if ((this.rut == null && other.rut != null) || (this.rut != null && !this.rut.equals(other.rut))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entity.Formulariocovid[ rut=" + rut + " ]";
    }
    
}
